'''
Created on Apr 2, 2018

@author: Willis Chen
'''
import datetime
import json
import jsonschema
import requests

from bs4 import BeautifulSoup
from definitions import currencies
from definitions import rtn_definitions as rtn
from definitions import settings
from jsonschema import validate
from urllib.parse import urlparse, urlencode


def get_airbnb_key_from_json_content_by_bs4(content):
    """ Retrieve airbnb key from web content with Beautiful Soup 4 framework.
    Args:
        content(str, required): Web content (e.g. <html>....</html>)
     Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {'airbnb_api_key':'d306zoyjsyarp7ifhu67rjxn52tv0t20'},
        }
        
    """
    try:
        soup = BeautifulSoup(content, "html.parser")
        bootstrap_layout = soup.find("meta", attrs={'id':'_bootstrap-layout-init'})
        airbnb_key_meta_dict = json.loads(bootstrap_layout.get('content'))
        json_validate_rtn_msg = jsonschema_validate(airbnb_key_meta_dict, settings.AIRBNB_API_KEY_SCHEMA)
    except:
        return rtn_msg(rtn.AIRBNB_API_KEY_NOT_EXIST, None)
    
    if json_validate_rtn_msg['status']['code'] != rtn.SUCCESS:
        return json_validate_rtn_msg 
    else:
        airbnb_api_key = airbnb_key_meta_dict['api_config']['key']
        return rtn_msg(rtn.SUCCESS, {'airbnb_api_key': airbnb_api_key})

     
def get_airbnb_key_from_url(url):
    """ Retrieve airbnb key from Airbnb room URL.
    Args:
        url(str, required): Airbnb room URL
     Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data':None or {'airbnb_api_key':'d306zoyjsyarp7ifhu67rjxn52tv0t20'},
        }
        
    """
    vld_airbnb_room_url_rtn = validate_airbnb_room_url_format(url)
    if vld_airbnb_room_url_rtn['status']['code'] != rtn.SUCCESS:
        return vld_airbnb_room_url_rtn
    else:
        url_vld_rtn, content = validate_url(url)
        if url_vld_rtn['status']['code'] == rtn.SUCCESS:
            return get_airbnb_key_from_json_content_by_bs4(content)
        else:
            return url_vld_rtn


def get_airbnb_calendar_info_by_daterange_and_url(url, currency, start_date, end_date):
    """Get Airbnb calendar information that includes room price, room available and room total count etc.
    Args:
        kwargs:
            url(str, required): Airbnb room URL
            currency(str, required): Currency(e.g. TWD)
            start_date(str, YYYY-MM-DD, optional): Start date
            end_date(str, YYYY-MM-DD, optional):  End date
            
    Returns:
        Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {
                    'airbnb_schedule_data':{
                        'total_count'(int): Room total count, 
                        'available'(bool): Room available, 
                        'price'(number): Room price, 
                        'currency'(str): Room currency,
                        },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.
                },
        }
            
    """
    airbnb_room_id_rtn = validate_airbnb_room_url_format(url)
    if airbnb_room_id_rtn['status']['code'] != rtn.SUCCESS:
        return airbnb_room_id_rtn
    else:
        return get_airbnb_calendar_info_by_daterange(airbnb_room_id_rtn['data']['airbnb_room_id'], currency, start_date, end_date)


def get_airbnb_calendar_info_by_daterange(airbnb_room_id, currency, start_date, end_date):
    """Get Airbnb calendar information that includes room price, room available and room total count etc.
    Args:
        kwargs:
            airbnb_room_id(str/int, required): Airbnb room id(int) 
            currency(str, required): Currency(e.g. TWD)
            start_date(str, YYYY-MM-DD, optional): Start date
            end_date(str, YYYY-MM-DD, optional):  End date
            
    Returns:
        Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {
                    'airbnb_schedule_data':{
                        'total_count'(int): Room total count, 
                        'available'(bool): Room available, 
                        'price'(number): Room price, 
                        'currency'(str): Room currency,
                        },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.  
                },
        }
            
    """
    return get_airbnb_calendar_info(airbnb_room_id=airbnb_room_id, currency=currency, start_date=start_date, end_date=end_date)


def get_airbnb_calendar_info_by_url(url, currency):
    """Get Airbnb calendar information that includes room price, room available and room total count etc by Airbnb room URL.
    Args:
        kwargs:
            url(str, required): Airbnb room URL
            currency(str, required): Currency(e.g. TWD)
    Returns:
        Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {
                    'airbnb_schedule_data':{
                        'total_count'(int): Room total counts, 
                        'available'(bool): Room available, 
                        'price'(number): Room price, 
                        'currency'(str): Room currency,
                        },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.  
                },
        }
            
    """
    airbnb_room_id_rtn = validate_airbnb_room_url_format(url)
    if airbnb_room_id_rtn['status']['code'] != rtn.SUCCESS:
        return airbnb_room_id_rtn
    else:
        return get_airbnb_calendar_info(airbnb_room_id=airbnb_room_id_rtn['data']['airbnb_room_id'], currency=currency)


def get_airbnb_calendar_info(**kwargs):
    """Get Airbnb calendar information that includes room price, room available and room total count etc.
       this function support two modes:
           1. Non date range mode: should set start date and end date (date range should be less than 330 days)
           2. date range mode: today -> expends 9 months 
    Args:
        kwargs:
            airbnb_room_id(str/int, required): Airbnb room id(int) 
            currency(str, required): Currency(e.g. TWD)
            airbnb_data_range(int, optional, default: 9 months): Data count by month count
            start_date(str, YYYY-MM-DD, optional): Start date
            end_date(str, YYYY-MM-DD, optional):  End date
            
    Returns:
        Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {
                    'airbnb_schedule_data':{
                        'total_count'(int): Room total count, 
                        'available'(bool): Room available, 
                        'price'(number): Room price, 
                        'currency'(str): Room currency,
                        },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.  
                },
        }
            
    """
    currency = kwargs['currency'] if 'currency' in kwargs else None
    airbnb_room_id = kwargs['airbnb_room_id'] if 'airbnb_room_id' in kwargs else None
    airbnb_data_range = kwargs['airbnb_data_range'] if 'airbnb_data_range' in kwargs.keys() else settings.ASIAYO_PUBLIC_DATA_MONTHS_RANGE
    start_date = kwargs['start_date'] if 'start_date' in kwargs else None
    end_date = kwargs['end_date'] if 'end_date' in kwargs else None
    str_date_format = '%Y-%m-%d'
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    
    if start_date != None or end_date != None:
        today = datetime.datetime.now().strftime("%Y-%m-%d")
        try:
            start_datetime = datetime.datetime.strptime(start_date, str_date_format)
            end_datetime = datetime.datetime.strptime(end_date, str_date_format)
            threshold_date = start_datetime + datetime.timedelta(days=settings.AIRBNB_DATERANGE_THRESHOLD)

            if start_datetime > end_datetime:
                return rtn_msg(rtn.DATE_RANGE_IS_INVALID, None)
            elif start_date < today or end_date < today:
                return rtn_msg(rtn.DATE_RANGE_IS_INVALID_CONDITION_LESS_THAN_TODAY, None)
            elif end_datetime > threshold_date:
                return rtn_msg(rtn.DATE_RANGE_IS_OVER_THRESHOLD, None)
            else:
                year = start_datetime.year
                month = end_datetime.month
                delta_count = 1
                
                # Calculates month count for Airbnb calendar API to avoid getting many Airbnb room data that not be used. 
                # This calculation is only for date range mode.
                if start_datetime.year == end_datetime.year:
                    airbnb_data_range = (end_datetime.month - start_datetime.month) + delta_count
                elif start_datetime.year < end_datetime.year:
                    airbnb_data_range = (12 - start_datetime.month) + (end_datetime.month) + delta_count
        except:
            return rtn_msg(rtn.DATE_RANGE_FORMAT_IS_INVALID_ISO_8601, None)
    

    key_rtn = get_airbnb_key_from_url(settings.AIRBNB_ROOM_URL+str(airbnb_room_id))
    if key_rtn['status']['code'] != rtn.SUCCESS:
        return key_rtn
    
    key = key_rtn['data']['airbnb_api_key']
    api_validation_rtn = request_airbnb_api(airbnb_room_id, currency, key)
    if api_validation_rtn['status']['code'] != rtn.SUCCESS:
        return api_validation_rtn
    else:
        airbnb_calendar_url = settings.AIRBNB_API_URL+'/calendar_months' 
        calendar_query_string_params = {
            'key': key,
            'currency': currency,
            'locale': 'en',
            'listing_id': airbnb_room_id,
            'month': month,
            'year': year,
            'count': airbnb_data_range,
            '_format': 'with_conditions',
            }
        
        url = airbnb_calendar_url+'?'+urlencode(calendar_query_string_params)
        url_validation_rtn = request_airbnb_api_by_url(url)
        
        if url_validation_rtn['status']['code'] != rtn.SUCCESS:
            return url_validation_rtn
        else:
            dict_content = json.loads(url_validation_rtn['data']['content'])
            today = datetime.datetime.now().strftime("%Y-%m-%d")
            try:
                for months in dict_content['calendar_months']: 
                    first_index = 0
                    while first_index < len(months['days']):
                        if months['days'][first_index]['date'] < today:
                           months['days'].remove(months['days'][first_index])
                        else:
                            raise StopIteration
            except StopIteration: pass
    
            json_validation_rtn = jsonschema_validate(dict_content, settings.CALENDAR_MONTHS_LIST_SCHEMA)
            if json_validation_rtn['status']['code'] != rtn.SUCCESS:
                return json_validation_rtn
            else:
                dict_data_rtn = {}
                all_available = True
                non_daterange_mode = start_date is None and end_date is None 
                for months in dict_content['calendar_months']: 
                    for days in months['days']:
                        if non_daterange_mode or (days['date'] >= start_date and days['date']<=end_date):
                            if not days['available']:
                                all_available = False

                            dict_data_rtn[days['date']] = {
                                                 'total_count': settings.AIRBNB_FIXED_DEFAULT_ROOM_COUNT, 
                                                 'available': days['available'], 
                                                 'price': days['price']['local_price'], 
                                                 'currency': days['price']['local_currency']}
                
                return rtn_msg(rtn.SUCCESS, {'airbnb_schedule_data':dict_data_rtn, 
                                             'all_available': all_available,
                                             'airbnb_api_url': url})
                

def get_airbnb_extra_info_by_url(url, currency):
    """ Get extra information of Airbnb room.
    Args:
        kwargs:
            airbnb_room_id(str, required): Airbnb room id
            currency(str required): Currency(e.g. TWD)
    Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {   'airbnb_extra_data':{
                            'currency': 幣別,  
                            'capacity': 總人數,
                            'guests': 基本人數,
                            'cleaning_fee': 清潔費,  
                            'extra_person_fee': 加人費用,  
                            'monthly_discount': 每月折扣, 
                            'weekly_discount': 每週折扣,
                            },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.
                },
        }
    """
    airbnb_room_id_rtn = validate_airbnb_room_url_format(url)
    if airbnb_room_id_rtn['status']['code'] != rtn.SUCCESS:
        return airbnb_room_id_rtn
    else:
        return get_airbnb_extra_info(airbnb_room_id=airbnb_room_id_rtn['data']['airbnb_room_id'], currency=currency)
    
def get_airbnb_extra_info(**kwargs):
    """ Get extra information of Airbnb room.
    Args:
        kwargs:
            airbnb_room_id(str, required): Airbnb room id
            currency(str required): Currency(e.g. TWD)
    Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': 
                {   'airbnb_extra_data':{
                            'currency': 幣別,  
                            'capacity': 總人數,
                            'guests': 基本人數,
                            'cleaning_fee': 清潔費,  
                            'extra_person_fee': 加人費用,  
                            'monthly_discount': 每月折扣, 
                            'weekly_discount': 每週折扣,
                            },
                    'all_available'(bool): All room are or not available.
                    'airbnb_api_url(str)': Airbnb API URL.
                },
        }
    """
    airbnb_room_id = kwargs['airbnb_room_id'] if 'airbnb_room_id' in kwargs else None
    currency = kwargs['currency'] if 'currency' in kwargs else 'TWD'
    key_rtn = get_airbnb_key_from_url(settings.AIRBNB_ROOM_URL+str(airbnb_room_id))
    if key_rtn['status']['code'] != rtn.SUCCESS:
        return key_rtn
    
    key = key_rtn['data']['airbnb_api_key']
    api_validation_rtn = request_airbnb_api(airbnb_room_id, currency, key, 'extra')
    if api_validation_rtn['status']['code'] != rtn.SUCCESS:
        return api_validation_rtn
    else:
        dict_content = json.loads(api_validation_rtn['data']['content'])
        json_validation_rtn = jsonschema_validate(dict_content, settings.LISTING_SCHEMA)
        if json_validation_rtn['status']['code'] != rtn.SUCCESS:
            return json_validation_rtn
        else:
            # 幣別
            currency = dict_content['listing']['native_currency']
            # 總人數
            capacity = dict_content['listing']['person_capacity']
            # 基本人數
            guests = dict_content['listing']['guests_included']
            # 清潔費
            cleaning_fee = dict_content['listing']['cleaning_fee_native']
            # 加人費用
            extra_person_fee = dict_content['listing']['price_for_extra_person_native']
            # 每月折扣
            monthly_discount =  dict_content['listing']['monthly_price_factor']
            # 每週折扣
            weekly_discount = dict_content['listing']['weekly_price_factor']
               
            result_rtn = {'currency': currency, 
                          'capacity': capacity, 
                          'guests': guests, 
                          'cleaning_fee': cleaning_fee, 
                          'extra_person_fee': extra_person_fee, 
                          'monthly_discount': monthly_discount, 
                          'weekly_discount': weekly_discount}
            return rtn_msg(rtn.SUCCESS, {'airbnb_extra_data': result_rtn,
                                         'airbnb_api_url': api_validation_rtn['data']['airbnb_api_url'],
                                         })
 
    
def jsonschema_validate(json, json_schema):
    """ Validate JSON schema
    Args:
        json(dictionary, required): JSON content
        json_schema(dictionary, required): JSON schema dedinitions
    Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None if successful, fail message of validation Otherwise.  
        }
        
    """
    try: 
        validate(json, json_schema)
        return rtn_msg(rtn.SUCCESS, None)
    except (jsonschema.exceptions.ValidationError, jsonschema.exceptions.SchemaError) as e:
        return rtn_msg(rtn.JSON_SCHEMA_VALIDATION_IS_FAIL, str(e))

 
def rtn_msg(code, data):
    """ JSON message of Airbnb API response.
    Args: 
        url(str): URL
    Returns:
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None / ...,
        }
        
    """
    return {
                'status':{
                    'code': code,
                    'msg': rtn.RETURN_CODE_MSG_MAPPINGS[code],
                    },
                'data': data,
            }

def request_airbnb_api(airbnb_room_id, currency, key, flag='calendar'):
    """ Request Airbnb API URL by Airbnb ID, Currency, Airbnb API Key and Airbnb API Type Flag.
    Args:
        airbnb_room_id (str, required): Airbnb room ID.
        currency(str, required): Currency of Airbnb room.
        key(str, required): Airbnb API key.
        flag(str, option): Airbnb API(Options: calendar[default], extra).
    Returns:
        content: None if unsuccessful, content of URL otherwise.
        rtn_msg: return message information
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None / 
                {    
                    'http_status_code': HTTP status code of Airbnb API request.
                    ''
                }
        }
    """
    if not airbnb_room_id or not key:
        return rtn_msg(rtn.AIRBNB_ROOM_ID_IS_EMPTY, None)
    elif not currency:
        return rtn_msg(rtn.AIRBNB_ROOM_CURRENCY_IS_EMPTY, None)

    currency_vld_rtn = validate_currency(currency)
    if currency_vld_rtn['status']['code'] != rtn.SUCCESS:
        return currency_vld_rtn

    month = datetime.datetime.now().month
    year = datetime.datetime.now().year
    url = None
    
    if flag == 'calendar':
        airbnb_calendar_url = settings.AIRBNB_API_URL+"/calendar_months" 
        calendar_query_string_params = {
            'key': key,
            'currency': currency,
            'locale': 'en',
            'listing_id': airbnb_room_id,
            'month': month,
            'year': year,
            'count': settings.AIRBNB_DATERANGE_THRESHOLD,
            '_format': 'with_conditions',
            }
        url = airbnb_calendar_url+'?'+urlencode(calendar_query_string_params)
    elif flag=='extra':
        airbnb_extra_url = settings.AIRBNB_API_URL+'/listings/'+str(airbnb_room_id)
        extra_query_string_params = {
            'key': key,
            '_format': 'v1_legacy_for_p3',
            'currency': currency,
            }
        url = airbnb_extra_url+'?'+urlencode(extra_query_string_params)
            
    return request_airbnb_api_by_url(url)


def request_airbnb_api_by_url(url):
        url_vld_rtn, content  = validate_url(url)
        http_status_code = url_vld_rtn['data']['http_status_code']
        if http_status_code == 200:
            code = rtn.SUCCESS
        elif http_status_code == 400:
            code = rtn.AIRBNB_ROOM_DATA_NOT_EXIST
        elif http_status_code == 404:
            code = rtn.AIRBNB_ROOM_NOT_EXIST
        elif http_status_code == 403:
            code = rtn.AIRBNB_ROOM_IS_CLOSE
        elif http_status_code in [503, 500]:
            code = rtn.AIRBNB_API_SERVER_IS_BUSY_NOT_AVAILABLE
        else:
            return url_vld_rtn
            
        # Convert Airbnb error code and message.
        url_vld_rtn['status']['code'] = code
        url_vld_rtn['status']['msg'] = rtn.RETURN_CODE_MSG_MAPPINGS[code]
        url_vld_rtn['data']['http_status_code'] = http_status_code
        url_vld_rtn['data']['content'] = content
        url_vld_rtn['data']['airbnb_api_url'] = url
        return url_vld_rtn


def validate_airbnb_room_url_format(url):
    """ Validate format of Airbnb room URL.
    Args:
        url (str): URL
    Returns:
        rtn_msg: return message information
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None / {'airbnb_room_id': ...}
        }
    """
    url_rtn = urlparse(url)
    url_path = url_rtn.path
    netloc_path = url_rtn.netloc
    # netloc_path : ????airbnb???
    if 'airbnb' not in netloc_path or not url_path:
        return rtn_msg(rtn.AIRBNB_URL_IS_INVALID, None)
    # Path data example: ['', 'rooms', '17867786']
    # index 0 : ''
    # index 1 : 'rooms'
    # index 2 : '17867786'
    url_path_data = url_path.split('/')
    # example: RoomS, rooms, ROOms etc.
    if url_path_data[1].lower() != 'rooms':
        return rtn_msg(rtn.AIRBNB_URL_IS_INVALID, None)     
    try:
        return rtn_msg(rtn.SUCCESS, {'airbnb_room_id': int(url_path_data[2])})
    except:
        return rtn_msg(rtn.AIRBNB_ROOM_ID_IS_INTEGER, None)
    
        
def validate_url(url):
    """ Validate URL.
    Args:
        url (str, required): URL
    Returns:
        content: None if unsuccessful, content of URL otherwise.
        rtn_msg: return message information
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None / {'http_status_code': ...}
        }
    """
    session = requests.session()
    session.cookies.clear()
    content = None
    if not url:
        return rtn_msg(rtn.URL_IS_EMPTY, None), None
    try:
        resp = session.get(url, timeout=settings.REQUESTS_TIMEOUT)
        if resp.status_code == 200:
            content = resp.text
            return rtn_msg(rtn.SUCCESS, {'http_status_code':resp.status_code}), content
        else:
            return rtn_msg(rtn.URL_IS_INVALID, {'http_status_code':resp.status_code}), content
    except requests.exceptions.Timeout:
        return rtn_msg(rtn.URL_RESPONSE_TIME_IS_EXPIRED, None), content
    except:
        return rtn_msg(rtn.URL_IS_INVALID, None), content
    finally:
        session.close()


def validate_currency(currency):
    """ Validate currency of Airbnb room.
    Args:
        currency (str, required): Currency of Airbnb room
    Returns:
        rtn_msg: return message information
        {
            'status':{
                'code': ...,
                'msg': ...,
                },
            'data': None
        }
    """
    if currency in currencies.CURRENCIES.keys():
        return rtn_msg(rtn.SUCCESS, None)
    else:
        return rtn_msg(rtn.AIRBNB_CURRENCY_IS_INVALID_ISO_4217, None)

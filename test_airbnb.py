'''
Created on Apr 3, 2018

@author: Willis Chen
'''


import unittest
import requests
from airbnb import airbnb_functions as airbnb_func
from definitions import rtn_definitions as rtn_def
from definitions import settings
from apigateway.document import export_api_doc

class AirbnbAPITestCase(unittest.TestCase):
    
    def get_test_msg(self, expected_code, result_code):
        return ('%s != %s'%(rtn_def.RETURN_CODE_MSG_MAPPINGS[expected_code], rtn_def.RETURN_CODE_MSG_MAPPINGS[result_code]))

    
    def get_http_status_code_url(self, code):
        # https://httpstat.us/
        # Above web site supports example URLs about different HTTP status codes.
        return "https://httpstat.us/%d"%(code)

    
    def get_expired_url(self):
        delta_seconds = 2 * 1000
        timeout_seconds = settings.REQUESTS_TIMEOUT * 1000
        expired_seconds = delta_seconds + timeout_seconds
        return "https://httpstat.us/200?sleep=%d"%expired_seconds

    
    def setUp(self):
        self.AIRBNB_API_CALENDAR_URL = 'https://www.airbnb.com/api/v2/calendar_months'
        self.AIRBNB_API_CALENDAR_PARAMS = '?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=TWD&listing_id=493845&locale=zh-TW&month=9&count=1&_format=with_conditions'
        self.AIRBNB_ROOM_URL = "https://www.airbnb.com.tw/rooms/%d"
        self.VALID_AIRBNB_ID = 17867786
        self.VALID_AIRBNB_URL = self.AIRBNB_ROOM_URL%(self.VALID_AIRBNB_ID)
        self.VALID_YEAR = 2020
        self.VALID_AIRBNB_API_URL = self.AIRBNB_API_CALENDAR_URL+self.AIRBNB_API_CALENDAR_PARAMS+'&year=%d'%self.VALID_YEAR
        self.INVALID_LONG_YEAR = 2100
        self.INVALID_400_AIRBNB_API_URL = self.AIRBNB_API_CALENDAR_URL+self.AIRBNB_API_CALENDAR_PARAMS+'&year=%d'%self.INVALID_LONG_YEAR
        self.INVALID_403_AIRBNB_ID = 21177383
        self.INVALID_403_AIRBNB_ROOM_URL = self.AIRBNB_ROOM_URL%(self.INVALID_403_AIRBNB_ID)
        self.LOCAL_PRICE_IS_ZERO_ID = 21089662
        self.INVALID_NOT_INT_ROOM_AIRBNB_URL = "https://www.airbnb.com.tw/rooms/dsadadada/"
        self.NON_AIRBNB_URL = "https://www.asiayo.com"
        self.LOCAL_PRICE_IS_ZERO_AIRBNB_API_URL = 'https://www.airbnb.com.tw/api/v2/calendar_months?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=USD&locale=zh-TW&listing_id=21089662&month=4&year=2018&count=9&_format=with_conditions'
    
    
    def test_jsonschema_validate_local_price_is_zero(self):
        content = requests.get(self.LOCAL_PRICE_IS_ZERO_AIRBNB_API_URL).content
        result = airbnb_func.jsonschema_validate(content, settings.CALENDAR_MONTHS_LIST_SCHEMA)
        self.assertEqual(rtn_def.JSON_SCHEMA_VALIDATION_IS_FAIL, result['status']['code'])
    
    
    def test_valid_ISO_4214(self):
        self.assertEqual(rtn_def.SUCCESS, airbnb_func.validate_currency('JPY')['status']['code'])

        
    def test_invalid_ISO_4214(self):
        result = airbnb_func.validate_currency('ASIAYO')
        self.assertEqual(rtn_def.AIRBNB_CURRENCY_IS_INVALID_ISO_4217, result['status']['code'])

        
    def test_valid_200_url(self):
        expected = airbnb_func.rtn_msg(rtn_def.SUCCESS, {'http_status_code':200})
        http_code_200_url = self.get_http_status_code_url(200)
        result, content = airbnb_func.validate_url(http_code_200_url)
        self.assertEqual(expected, result)
        self.assertNotEqual(content, None)

    
    def test_invalid_400_url(self):
        code = 400
        http_code_400_url = self.get_http_status_code_url(code)
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code':code})
        result, content = airbnb_func.validate_url(http_code_400_url)
        self.assertEqual(expected, result)
    
    
    def test_invalid_400_airbnb_url(self):
        code = 400
        http_code_400_url = self.get_http_status_code_url(code)
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code':code})
        result, content = airbnb_func.validate_url(self.INVALID_400_AIRBNB_API_URL)
        self.assertEqual(expected, result)
        
        
    def test_invalid_403_url(self):
        code = 403
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code':code})
        result, content = airbnb_func.validate_url(self.get_http_status_code_url(code))
        self.assertEqual(expected, result)
        self.assertEqual(content, None)

        
    def test_invalid_404_url(self):
        code = 404
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code': code})
        result, content = airbnb_func.validate_url(self.get_http_status_code_url(code))
        self.assertEqual(expected, result)
        self.assertEqual(content, None)

        
    def test_invalid_500_url(self):
        code = 500
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code': code})
        result, content = airbnb_func.validate_url(self.get_http_status_code_url(code))
        self.assertEqual(expected, result)
        self.assertEqual(content, None)

        
    def test_invalid_503_url(self):
        code = 503
        expected = airbnb_func.rtn_msg(rtn_def.URL_IS_INVALID, {'http_status_code': code})
        result, content = airbnb_func.validate_url(self.get_http_status_code_url(code))
        self.assertEqual(expected, result)
        self.assertEqual(content, None)

        
    def test_expired_url(self):
        excepted = airbnb_func.rtn_msg(rtn_def.URL_RESPONSE_TIME_IS_EXPIRED, None)
        result, content = airbnb_func.validate_url(self.get_expired_url())
        self.assertEqual(excepted, result)
        self.assertEqual(content, None)

        
    def test_airbnb_api_key_with_airbnb_url(self):
        excepted = airbnb_func.rtn_msg(rtn_def.SUCCESS, {'airbnb_api_key': 'd306zoyjsyarp7ifhu67rjxn52tv0t20'})
        content = requests.get(self.VALID_AIRBNB_URL).content
        result = airbnb_func.get_airbnb_key_from_json_content_by_bs4(content)
        self.assertEqual(excepted, result)

        
    def test_airbnb_api_key_with_invalid_airbnb_url(self):
        excepted = airbnb_func.rtn_msg(rtn_def.SUCCESS, {'airbnb_api_key': 'd306zoyjsyarp7ifhu67rjxn52tv0t20'})
        content = requests.get(self.INVALID_NOT_INT_ROOM_AIRBNB_URL).content
        result = airbnb_func.get_airbnb_key_from_json_content_by_bs4(content)
        self.assertEqual(excepted, result)

        
    def test_airbnb_api_key_with_non_airbnb_url(self):
        excepted = rtn_def.AIRBNB_API_KEY_NOT_EXIST
        content = requests.get(self.NON_AIRBNB_URL).content
        result = airbnb_func.get_airbnb_key_from_json_content_by_bs4(content)['status']['code']
        self.assertEqual(excepted, result)

        
    def test_airbnb_room_url_format(self):
        excepted = airbnb_func.rtn_msg(rtn_def.SUCCESS, {'airbnb_room_id': self.VALID_AIRBNB_ID})
        result = airbnb_func.validate_airbnb_room_url_format(self.VALID_AIRBNB_URL)
        self.assertEqual(excepted, result)

        excepted = airbnb_func.rtn_msg(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, None)
        result = airbnb_func.validate_airbnb_room_url_format(self.INVALID_NOT_INT_ROOM_AIRBNB_URL)
        self.assertEqual(excepted, result)

        
    def test_request_airbnb_api_200(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.request_airbnb_api(self.VALID_AIRBNB_ID, 'TWD', 'd306zoyjsyarp7ifhu67rjxn52tv0t20')
        self.assertEqual(excepted, result['status']['code'])

    
    def test_request_airbnb_api_403(self):
        result = airbnb_func.request_airbnb_api(self.INVALID_403_AIRBNB_ID, 'TWD', 'd306zoyjsyarp7ifhu67rjxn52tv0t20')
        self.assertEqual(rtn_def.AIRBNB_ROOM_IS_CLOSE, result['status']['code'])

    
    def test_airbnb_api_by_url_status_400(self):
        excepted = rtn_def.AIRBNB_ROOM_DATA_NOT_EXIST
        result = airbnb_func.request_airbnb_api_by_url(self.INVALID_400_AIRBNB_API_URL)
        self.assertEqual(excepted, result['status']['code'])

        
    def test_airbnb_extra_info_by_airbnb_room_id(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.get_airbnb_extra_info(airbnb_room_id=self.VALID_AIRBNB_ID, currency='TWD')
        self.assertEqual(excepted, result['status']['code'])
        result = airbnb_func.get_airbnb_extra_info(airbnb_room_id=self.VALID_AIRBNB_URL, currency='TWD')
        self.assertEqual(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code'], self.get_test_msg(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code']))

    
    def test_airbnb_extra_info_by_airbnb_url(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.get_airbnb_extra_info_by_url(self.VALID_AIRBNB_URL, 'TWD')
        self.assertEqual(excepted, result['status']['code'])

        
    def test_valid_airbnb_calendar_info(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.get_airbnb_calendar_info(airbnb_room_id=self.VALID_AIRBNB_ID, currency='TWD')
        self.assertEqual(excepted, result['status']['code'])

        
    def test_local_price_is_zero_airbnb_calendar_info_before_today(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.get_airbnb_calendar_info(airbnb_room_id=self.LOCAL_PRICE_IS_ZERO_ID, currency='TWD')
        self.assertEqual(excepted, result['status']['code'])

    
    def test_valid_airbnb_calendar_info_by_url(self):
        excepted = rtn_def.SUCCESS
        result = airbnb_func.get_airbnb_calendar_info_by_url(self.VALID_AIRBNB_URL, 'TWD')
        self.assertEqual(excepted, result['status']['code'], self.get_test_msg(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code']))
        

    def test_invalid_airbnb_calendar_info_by_url(self):
        excepted = rtn_def.AIRBNB_ROOM_ID_IS_INTEGER
        result = airbnb_func.get_airbnb_calendar_info_by_url(self.INVALID_NOT_INT_ROOM_AIRBNB_URL, 'TWD')
        self.assertEqual(excepted, result['status']['code'])


    def test_airbnb_calendar_info_by_daterange(self): 
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018-08-28', '2018-08-30')
        self.assertEqual(rtn_def.SUCCESS, result['status']['code'],self.get_test_msg(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code']))
        
        excepted = rtn_def.DATE_RANGE_FORMAT_IS_INVALID_ISO_8601
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018/08/07', '2018/08/18')
        self.assertEqual(excepted, result['status']['code'],  self.get_test_msg(excepted, result['status']['code']))
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018-08-07', '2018/08/18')
        self.assertEqual(excepted, result['status']['code'])
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018-08-09', '2018-08-07')
        self.assertEqual(rtn_def.DATE_RANGE_IS_INVALID, result['status']['code'])
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018-08-09', '2019-08-07')
        self.assertEqual(rtn_def.DATE_RANGE_IS_OVER_THRESHOLD, result['status']['code'])
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'ASIAYO', '2018-08-28', '2018-08-30')
        self.assertEqual(rtn_def.AIRBNB_CURRENCY_IS_INVALID_ISO_4217, result['status']['code'],self.get_test_msg(rtn_def.AIRBNB_CURRENCY_IS_INVALID_ISO_4217, result['status']['code']))
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2018-08-28', '2019-08-30')
        self.assertEqual(rtn_def.DATE_RANGE_IS_OVER_THRESHOLD, result['status']['code'])
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_URL, 'TWD', '2018-08-28', '2018-08-30')
        self.assertEqual(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code'], self.get_test_msg(rtn_def.AIRBNB_ROOM_ID_IS_INTEGER, result['status']['code']))
        
        result = airbnb_func.get_airbnb_calendar_info_by_daterange(self.VALID_AIRBNB_ID, 'TWD', '2030-09-28', '2030-09-30')
        self.assertEqual(rtn_def.AIRBNB_ROOM_DATA_NOT_EXIST, result['status']['code'], self.get_test_msg(rtn_def.AIRBNB_ROOM_DATA_NOT_EXIST, result['status']['code']))
        
    def test_export_error_code(self):
        result = export_api_doc.export_code_and_msg()

        
if __name__ == '__main__':
    unittest.main()
#!/bin/bash

AWS_PROFILE=asiayo
export AWS_PROFILE=asiayo
printenv

# dev:   ./deploy.sh
# prod:  ./deploy.sh prod
STAGE=${1:-dev}
PROJECT=airbnbapi-${STAGE}


# Change the suffix on the bucket to something unique!
BUCKET=ay-airbnb-api-bucket-${STAGE}

# make a build directory to store artifacts
rm -rf build
rm -rf apigateway/document/swagger
mkdir build
mkdir build/src
mkdir apigateway/document/swagger
mkdir apigateway/document/swagger/yaml
mkdir apigateway/document/swagger/json

cp -rf apigateway build/src
cp -rf airbnb build/src
cp -rf definitions build/src
cp -rf lambda build/src
cp -rf requirements.txt build/src
 
pip install -r build/src/requirements.txt -t build/src

# make the deployment bucket in case it doesn't exist
aws s3 mb s3://$BUCKET

# generate next stage yaml file
aws cloudformation package                   \
    --template-file template.yaml            \
    --output-template-file build/output.yaml \
    --s3-bucket $BUCKET                      

# the actual deployment step
aws cloudformation deploy                     \
    --template-file build/output.yaml         \
    --stack-name $PROJECT                     \
    --capabilities CAPABILITY_IAM             \
    --parameter-overrides Environment=$STAGE

REST_API_ID=$(aws apigateway get-rest-apis | python3 -c "import sys, json; print([d['id']  for d in json.load(sys.stdin)['items'] if d['name']=='${PROJECT}'][0])")

# exports swagger file to local folder 
aws apigateway get-export --parameters extensions='postman' --rest-api-id $REST_API_ID --stage-name $STAGE --export-type swagger --accepts application/json apigateway/document/swagger/json/airbnb_swagger.json
aws apigateway get-export --parameters extensions='postman' --rest-api-id $REST_API_ID --stage-name $STAGE --export-type swagger --accepts application/yaml apigateway/document/swagger/yaml/airbnb_swagger.yaml

# packages swagger files into deployment file.
cp -rf apigateway build/src
 
pip install -r build/src/requirements.txt -t build/src

# deploy again
# generate next stage yaml file
aws cloudformation package                   \
    --template-file template.yaml            \
    --output-template-file build/output.yaml \
    --s3-bucket $BUCKET                      

# the actual deployment step
aws cloudformation deploy                     \
    --template-file build/output.yaml         \
    --stack-name $PROJECT                     \
    --capabilities CAPABILITY_IAM             \
    --parameter-overrides Environment=$STAGE

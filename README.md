### What is this repository for? ###
* This repository is convenient for accessing Airbnb room data and validating json format of Airbnb data.

### Which tools should you know?
* Python3.5
* Virtualenv
* AWS CLI, AWS IAM Policy, AWS lambda, AWS API Gateway, AWS CloudFormation, AWS key access
* Swagger description

### Which python frameworks would be uesed?
Please read requirements.txt

### How to ues? ###
You would follow steps to install this repository to your AWS service if assumes you already know above.
```
git clone <<repository link>>
modifies deploy.sh for your configuration.
cd airbnb-api
virtualenv -p python3 env
source env/bin/activate
./deploy.sh <stage, e.g. test, delta, beta or prod>
```

### Workaround ###
* Uses AWS CloudFormation stack to manage project stage.
* Uses AWS CloudFormation Web platform to configurate domain name.

### Which isssue would be improved? ###
* Uses AWS CloudFormation template to configurate AWS::APIGateway::Stage / AWS::APIGateway::Deployment for being convient to manage project stage.
* Uses AWS CloudFormation template to configurate AWS::APIGateway::DomainName for being covenient to assign domain name of AsiaYo to project.
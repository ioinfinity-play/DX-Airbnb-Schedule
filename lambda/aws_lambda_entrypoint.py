import json
from airbnb import airbnb_functions
from apigateway.document import export_api_doc
from definitions import settings, rtn_definitions as rtn_def


def airbnb_calendar_info(event, context):
    airbnb_room_id = get_path_param_val(event, 'airbnb_room_id')
    currency = get_path_param_val(event, 'currency')
    msg_rtn = airbnb_functions.get_airbnb_calendar_info(airbnb_room_id=airbnb_room_id, currency=currency)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403

    return resp_msg(code, msg_rtn)


def airbnb_calendar_info_by_daterange(event, context):
    airbnb_room_id = get_path_param_val(event, 'airbnb_room_id')
    currency = get_path_param_val(event, 'currency')
    start_date = get_path_param_val(event, 'start_date')
    end_date = get_path_param_val(event, 'end_date')
    
    msg_rtn = airbnb_functions.get_airbnb_calendar_info_by_daterange(airbnb_room_id, currency, start_date, end_date)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403

    return resp_msg(code, msg_rtn)


def airbnb_calendar_info_by_url(event, context):
    airbnb_room_url = get_query_str_param_val(event, 'airbnb_room_url')
    currency = get_query_str_param_val(event, 'currency')
    msg_rtn = airbnb_functions.get_airbnb_calendar_info_by_url(airbnb_room_url, currency)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403
    return resp_msg(code, msg_rtn)


def airbnb_calendar_info_by_url_and_daterange(event, context):
    airbnb_room_url = get_query_str_param_val(event, 'airbnb_room_url')
    currency = get_query_str_param_val(event, 'currency')
    start_date = get_query_str_param_val(event, 'start_date')
    end_date = get_query_str_param_val(event, 'end_date')
    code = 200
    msg_rtn = airbnb_functions.get_airbnb_calendar_info_by_daterange_and_url(airbnb_room_url, currency, start_date, end_date)
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403
    return resp_msg(code, msg_rtn)


def airbnb_extra_info(event, context):
    airbnb_room_id = get_path_param_val(event, 'airbnb_room_id')
    currency = get_path_param_val(event, 'currency')
    msg_rtn = airbnb_functions.get_airbnb_extra_info(airbnb_room_id= airbnb_room_id, currency=currency)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403
    return resp_msg(code, msg_rtn) 


def airbnb_extra_info_by_url(event, context):
    airbnb_room_url = get_query_str_param_val(event, 'airbnb_room_url')
    currency = get_query_str_param_val(event, 'currency')
    msg_rtn = airbnb_functions.get_airbnb_extra_info_by_url(airbnb_room_url, currency)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403
    return resp_msg(code, msg_rtn)


def aibnb_export_api_doc(event, context):
    return resp_msg(200, export_api_doc.export())


def aibnb_export_api_code_and_msg(evnet, context):
    return resp_msg(200, export_api_doc.export_code_and_msg())


def airbnb_validate_url(event, context):
    url = get_query_str_param_val(event, 'url')
    msg_rtn, _ = airbnb_functions.validate_url(url)
    code = 200
    if msg_rtn['status']['code'] != rtn_def.SUCCESS:
        code = 403
    return resp_msg(code, msg_rtn)


def get_header_val(event, event_arg):
    val = None
    if event["headers"] is not None:
        if event_arg in event["headers"]:
            val = event["headers"][event_arg]
    return val


def get_path_param_val(event, event_arg):
    val = None
    if event["pathParameters"] is not None:
        if event_arg in event["pathParameters"]:
            val = event["pathParameters"][event_arg]
    return val


def get_query_str_param_val(event, event_arg):
    val = None
    if event["queryStringParameters"] is not None:
        if event_arg in event["queryStringParameters"]:
            val = event["queryStringParameters"][event_arg]
    return val


def resp_msg(code, data):
    return {
            "statusCode": code,
            "body": json.dumps(data),
            "headers": settings.RESPONSE_HEADERS,
        }

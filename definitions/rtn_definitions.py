from . import settings
# General
SUCCESS = 0
DATE_RANGE_IS_INVALID = 1000
DATE_RANGE_FORMAT_IS_INVALID_ISO_8601 = 1001
DATE_RANGE_IS_OVER_THRESHOLD = 1002
JSON_SCHEMA_VALIDATION_IS_FAIL = 1003
URL_IS_EMPTY = 1004
URL_IS_INVALID = 1005
URL_RESPONSE_TIME_IS_EXPIRED = 1006
UNKNOWN_REASON = 1007
AIRBNB_CURRENCY_IS_INVALID_ISO_4217 = 1008
DATE_RANGE_IS_INVALID_CONDITION_LESS_THAN_TODAY= 1009

# Airbnb
AIRBNB_API_KEY_NOT_EXIST = 2000
AIRBNB_API_KEY_IS_EMPTY = 2001
AIRBNB_API_RESPONSE_TIME_IS_EXPIRED = 2002
AIRBNB_API_SERVER_IS_BUSY_NOT_AVAILABLE = 2003
AIRBNB_ROOM_CURRENCY_IS_EMPTY = 2004
AIRBNB_ROOM_ID_IS_EMPTY = 2005
AIRBNB_ROOM_ID_IS_INTEGER = 2006
AIRBNB_ROOM_IS_CLOSE = 2007
AIRBNB_ROOM_NOT_EXIST = 2008
AIRBNB_URL_IS_INVALID = 2009
AIRBNB_ROOM_DATA_NOT_EXIST = 2010

# Error code maps to error message.
RETURN_CODE_MSG_MAPPINGS = {
    AIRBNB_API_KEY_NOT_EXIST: 'API key of Airbnb not exist.',
    AIRBNB_API_KEY_IS_EMPTY: 'API key is empty.',
    AIRBNB_API_RESPONSE_TIME_IS_EXPIRED: 'Response time of Airbnb API is expired.',
    AIRBNB_API_SERVER_IS_BUSY_NOT_AVAILABLE : 'Airbnb API server is busy / not available.',
    AIRBNB_CURRENCY_IS_INVALID_ISO_4217: 'Airbnb currency is invalid, please refer to  currency format of ISO 4217.', 
    AIRBNB_ROOM_ID_IS_EMPTY: 'Airbnb room id is empty.',
    AIRBNB_ROOM_ID_IS_INTEGER: 'Airbnb room id is integer.',
    AIRBNB_ROOM_CURRENCY_IS_EMPTY: 'Airbnb room currency is empty.',
    AIRBNB_ROOM_IS_CLOSE: 'Airbnb room is close.',
    AIRBNB_ROOM_NOT_EXIST: 'Airbnb room not exist.',
    AIRBNB_ROOM_DATA_NOT_EXIST: 'Airbnb room data not exist',
    AIRBNB_URL_IS_INVALID: 'Airbnb url is invalid.',
    DATE_RANGE_IS_INVALID: 'Date range is invalid.',
    DATE_RANGE_FORMAT_IS_INVALID_ISO_8601: 'Date range format is invalid, please refer to date format of ISO 8601.',
    DATE_RANGE_IS_INVALID_CONDITION_LESS_THAN_TODAY: 'Start date and end date should be greater than / equal to today.',
    DATE_RANGE_IS_OVER_THRESHOLD: 'Date range is over threshold - %d days.'%(settings.AIRBNB_DATERANGE_THRESHOLD),
    JSON_SCHEMA_VALIDATION_IS_FAIL: 'Result of json schema validation is fail.',
    SUCCESS: 'Success',
    URL_IS_EMPTY: 'URL is empty.',
    URL_IS_INVALID: 'URL is invalid.',
    URL_RESPONSE_TIME_IS_EXPIRED: 'URL response time is expired.',
    UNKNOWN_REASON: 'Unkown reason.',
    }
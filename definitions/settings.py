'''
Created on Apr 2, 2018

@author: Willis Chen
'''

AIRBNB_API_URL = 'https://www.airbnb.com/api/v2'
AIRBNB_ROOM_URL = 'https://www.airbnb.com/rooms/'
AIRBNB_FIXED_DEFAULT_ROOM_COUNT = 1

# AsiaYo Public Date Range.
ASIAYO_PUBLIC_DATA_MONTHS_RANGE = 9

# Valid URL of Specific Airbnb ROOM 
AIRBNB_SPECIFIC_VALID_ROOM_URL = 'https://www.airbnb.com/rooms/17867786'

 
# Airbnb API example link:
#    https://www.airbnb.com/api/v2/calendar_months
#        ?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=TWD
#        &locale=zh-TW&listing_id=493845&month=5
#        &day=1&year=2018&count=12&_format=with_conditions
# Airbnb API parameters definitions:
#     month: begin month
#     count: data count in x months, unit: month count.
# Airbnb API restriction:
#     max data count of Airbnb data is about 365 days.
#     available data count is about 334(365-31) 
# AIRBNB_DATERANGE_THRESHOLD should set x days
AIRBNB_DATERANGE_THRESHOLD = 330


# Second
REQUESTS_TIMEOUT = 10

# CORS
RESPONSE_HEADERS = {
    "Access-Control-Allow-Headers": "Origin,X-Auth-Token,X-Requested-With,Token,Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
    "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json"
    }

# JSON Schema Validation
SCHEMA_DRAFT_VERSION = "http://json-schema.org/draft-04/schema#"
DEFINITIONS_SCHEMA = {
            "non-empty-string": {
                "type": "string",
                "minLength": 1
            },
            "non-zero-positive-number": {
                "type": "number",
                "minimum": 1,
            },
            "non-zero-price-number": {
                "type": ["number"],
                "minimum": 1,
            },
            "price-number": {
                "type": ["number", "null"],
                "minimum": 0,
            },
            "date-time-YYYY-MM-DD": {
                "type": "string",
                "minLength": 1,
                "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}$",
            },
        }

AIRBNB_API_KEY_SCHEMA = {
        "$schema": SCHEMA_DRAFT_VERSION,
        "type" : "object",
        "properties" : {
                "api_config" : {
                        "type" : "object",
                        "properties" : {
                            "key": {"$ref": "#/definitions/non-empty-string"},
                            "baseUrl": {"$ref": "#/definitions/non-empty-string"},
                        },
                        "required":["key", "baseUrl"]
                    }
            },
        "required": ["api_config"],
        "definitions": DEFINITIONS_SCHEMA,
        }

# Airbnb API - Listing - Extra Price
LISTING_EXTRA_INFO_SCHEMA = {
    "type" : "object",
    "properties" : {
            "native_currency" : {"$ref": "#/definitions/non-empty-string"},
            "person_capacity": {"$ref": "#/definitions/non-zero-positive-number"},
            "guests_included": {"$ref": "#/definitions/non-zero-positive-number"},
            "cleaning_fee_native": {"$ref": "#/definitions/price-number"},
            "price_for_extra_person_native": {"$ref": "#/definitions/price-number"},
            "monthly_price_factor": {"type" : ["number", "null"]},
            "weekly_price_factor": {"type" : ["number", "null"]},
            },
    "required": ["native_currency", "person_capacity", "guests_included", "cleaning_fee_native", "price_for_extra_person_native", "monthly_price_factor", "weekly_price_factor"],
    }


LISTING_SCHEMA = {
    "$schema": SCHEMA_DRAFT_VERSION,
    "type": "object",
    "properties" : {
            "listing" : LISTING_EXTRA_INFO_SCHEMA,
            },
    "definitions": DEFINITIONS_SCHEMA,
    }

# Airbnb API - Schedule Price & Available
CALENDAR_MONTHS_DAYS_DAY_SCHEMA = {
        "type" : "object",
        "properties" : {
                "available" : {"type" : "boolean"},
                "price" : {
                        "type" : "object",
                        "properties" : {
                            "local_price": {"$ref": "#/definitions/non-zero-price-number"},
                            "date" : {"$ref": "#/definitions/date-time-YYYY-MM-DD"},
                            "local_currency": {"$ref": "#/definitions/non-empty-string"},
                        },
                        "required":["local_price", "date", "local_currency"]
                    }
            },
        "required": ["available", "price"],
        }
    
CALENDAR_MONTHS_DAYS_SCHEMA = {
        "type": "array",
        "items": CALENDAR_MONTHS_DAYS_DAY_SCHEMA,
        }
    
CALENDAR_MONTH_SCHEMA = { 
        "type" : "object",
        "properties" : {
                "abbr_name":{"$ref": "#/definitions/non-empty-string"},
                "days":CALENDAR_MONTHS_DAYS_SCHEMA
            },
        "required": ["abbr_name", "days"],
        }
    
CALENDAR_MONTHS_LIST_SCHEMA = {
        "$schema": SCHEMA_DRAFT_VERSION,
        "type" : "object",
        "properties" : {
                "calendar_months":{
                    "type":"array",
                    "items": CALENDAR_MONTH_SCHEMA
                    },
            },
        "required": ["calendar_months"],
        "definitions": DEFINITIONS_SCHEMA,
        }


REST_API_ID='awznwm0gx7'
